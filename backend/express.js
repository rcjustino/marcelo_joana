import express from 'express'
import * as fs from 'fs/promises'

const server = express();
const port = 3001;

// URI para disponibilizar a resposta do num 1
server.get('/num', async (req, res) => {
    try {
        const num = 1
        res.status(200).json(num)
    } catch(ex) {
        res.status(500).send('Erro')
    }
})

server.use(express.json());
server.listen(port, () => console.log('Ready on ' + port));