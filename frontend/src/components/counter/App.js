import React from 'react';
import './App.css';
import Segunda from './segunda.js'
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numPag: 0,
      counter: 0,
      valueToUpdate: ''
    }
    this.substitui = this.substitui.bind(this);
    this.incrementa = this.incrementa.bind(this);
    this.decrementa = this.decrementa.bind(this);
    this.servidorNum = this.servidorNum.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    console.log(event)
    this.setState({ valueToUpdate: Number(event.target.value) })
  }

  substitui() {
    this.setState({ counter: this.state.valueToUpdate, valueToUpdate: '' })
  }

  async incrementa() {
    this.setState({ counter: this.state.valueToUpdate + this.state.counter, valueToUpdate: '', numPag: 0})
  }

  async decrementa() {
    this.setState({ counter: this.state.counter - this.state.valueToUpdate, valueToUpdate: '', numPag : 1})
  }

  async servidorNum() {
    await fetch('/num')
    .then(res => res.json())
    .then(prom => this.setState({ counter: prom}))
  }

  render() {
    if (this.state.numPag === 0) {
      return (
        <div className="App">
          <header className="App-header">
  
            <h2>{this.state.counter}</h2>
  
            <p>
              Insira um número para mudar o contador:
            </p>
            <input type="number" value={this.state.valueToUpdate} onChange={this.handleChange} />
  
            <button onClick={this.substitui} width='200px'>Substitui</button>
            <button onClick={this.incrementa} width='200px'>Incrementa</button>
            <button onClick={this.decrementa} width='200px'>Decrementa</button>
            <button onClick={this.servidorNum} width='200px'>API</button>
          </header>
        </div>
      );
    } else {
      return (
        <Segunda contadorSegunda={this.state.counter} funcaoIncrementa={() => this.incrementa()}/>
      );
    }
  }

}