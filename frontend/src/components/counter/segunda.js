import React from 'react';
import './App.css';

export default class Segunda extends React.Component {
    // constructor(props) {
    //     super(props);
    //   }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <p>
                        Segunda pagina contador:
                    </p>

                    <p>{this.props.contadorSegunda}</p>

                    <button onClick={this.props.funcaoIncrementa}></button>
                </header>
            </div>
        );
    }
}