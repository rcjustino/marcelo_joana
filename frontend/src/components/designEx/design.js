import React from 'react';
import './design.css';
import { MdHome, MdAccountCircle, MdRssFeed, MdDevices, MdCloudUpload } from "react-icons/md";

// Scope global -> Declaramos as variaveis constantes que sao usadas multiplas vezes
const API_KEY = '05de9fbdef46d314b1a413cb95d43018';
export default class Design extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            theme: '',
            city: 'Lisbon',
            country: 'PT',
            dataTempo : '',
            clouds: 0
        }
        this.switchTheme = this.switchTheme.bind(this);
    }

    componentDidMount() {
        // verifica se temos um dark ou light theme no broswer e faz adjust à aplicacao
        const defaultDark = window.matchMedia('(prefers-color-scheme: dark)').matches; 
        this.setState({ theme: defaultDark ? 'Dark' : 'Light'})
        
        try {
            fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${this.state.city},${this.state.country}&units=metric&appid=${API_KEY}`)
            .then(response => response.json()) // parse json
            .then(res => {
                console.log(res);
                this.setState({dataTempo : res.list[0].main.temp, clouds : res.list[0].clouds.all });
            })
        } catch(err) { 
            console.error(err)
        }
        
    }

    switchTheme() {
       const theme = this.state.theme === 'Light' ? 'Dark' : 'Light';
       this.setState({ theme: theme});
    }

    render() {
        return (
            <div className='App' data-theme={this.state.theme}>
                <div className='Block'>
                    <h1>Olá Dark Mode</h1>
                    <button onClick={this.switchTheme}>Switch Theme</button>
                </div>
                <div className='Block'>
                    <img src='logo_round.png' className='image' alt=''/>
                    <p>{this.state.dataTempo}ºC</p>
                    <p>{this.state.clouds < 15 ? 'No Clouds' : 'Cloudy'}</p>
                </div>
                <footer className='Footer'>
                    <button className='Buttons'><MdHome className='Icons'/></button>
                    <button className='Buttons'><MdAccountCircle className='Icons'/></button>
                    <button className='Buttons'><MdRssFeed className='Icons'/></button>
                    <button className='Buttons'><MdDevices className='Icons'/></button>
                    <button className='Buttons'><MdCloudUpload className='Icons'/></button>
                </footer>
            </div>
        );
    }
}